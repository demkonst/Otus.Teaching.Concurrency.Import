﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal class Program
    {
        private const string GeneratorAppPath = @"../../../../Otus.Teaching.Concurrency.Import.DataGenerator.App/bin/Debug/netcoreapp5.0/Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";

        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? string.Empty, "customers.xml");
        private static GeneratorMode _generatorMode = GeneratorMode.Method;

        private static void Main(string[] args)
        {
            if (args != null)
            {
                if (args.Length >= 1)
                {
                    _generatorMode = Enum.Parse<GeneratorMode>(args[0], true);
                }

                if (args.Length >= 2)
                {
                    _dataFilePath = args[1];
                }
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();


            var customers = new XmlParser(_dataFilePath).Parse();

            new AppDbContext().Database.Migrate();

            new CustomerRepository().Reset();
            
            var loader = new FakeDataLoader(customers);
            loader.LoadData();
        }

        private static void GenerateCustomersDataFile()
        {
            switch (_generatorMode)
            {
                case GeneratorMode.Method:
                    var xmlGenerator = new XmlGenerator(_dataFilePath, 1_000_000);
                    xmlGenerator.Generate();
                    break;
                case GeneratorMode.Process:
                    var process = Process.Start(GeneratorAppPath, _dataFilePath);
                    process?.WaitForExit();
                    break;
            }
        }

        internal enum GeneratorMode
        {
            Method = 1,
            Process = 2
        }
    }
}
