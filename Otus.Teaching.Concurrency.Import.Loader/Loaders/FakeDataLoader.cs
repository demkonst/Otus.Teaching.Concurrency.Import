using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class FakeDataLoader: IDataLoader
    {
        private const int ThreadsCount = 6;
        private readonly List<Customer> _customers;

        public FakeDataLoader(List<Customer> customers)
        {
            _customers = customers;
        }

        public void LoadData()
        {
            var sw = new Stopwatch();

            Console.WriteLine("Loading data...");

            sw.Start();

            var partSize = _customers.Count / ThreadsCount;

            var handlers = new List<WaitHandle>();

            for (var i = 0; i < ThreadsCount; i++)
            {
                var handler = new AutoResetEvent(false);
                handlers.Add(handler);

                var query = _customers.OrderBy(x => x.Id).Skip(partSize * i);
                if (i != ThreadsCount - 1)
                {
                    query = query.Take(partSize);
                }

                ThreadPool.QueueUserWorkItem(LoadData, new StateWrapper
                {
                    Customers = query.ToList(),
                    WaitHandle = handler
                });
            }

            WaitHandle.WaitAll(handlers.ToArray());

            sw.Stop();

            Console.WriteLine($"Loaded data. Took {sw.Elapsed.TotalSeconds:F}");
        }

        private static void LoadData(object state)
        {
            var ctx = new CustomerRepository();
            if (!(state is StateWrapper stateWrapper))
            {
                return;
            }

            foreach (var customer in stateWrapper.Customers)
            {
                ctx.AddCustomer(customer);
            }

            ctx.SaveChanges();

            stateWrapper.WaitHandle.Set();
        }

        internal struct StateWrapper
        {
            public AutoResetEvent WaitHandle { get; set; }
            public List<Customer> Customers { get; set; }
        }
    }
}
