﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Dto;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser: IDataParser<List<Customer>>
    {
        private readonly string _path;

        public XmlParser(string path)
        {
            _path = path;
        }

        public List<Customer> Parse()
        {
            Console.WriteLine("Parsing started...");

            CustomersList result = null;
            using var stream = File.OpenRead(_path);
            {
                var xml = new XmlSerializer(typeof(CustomersList), new XmlRootAttribute("Customers")).Deserialize(stream);
                if (xml is CustomersList customersList)
                {
                    result = customersList;
                }
            }

            Console.WriteLine("Parsing finished...");

            return result?.Customers;
        }
    }
}
