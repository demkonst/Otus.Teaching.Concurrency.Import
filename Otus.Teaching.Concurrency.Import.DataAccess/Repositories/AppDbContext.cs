using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class AppDbContext: DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=tcp:localhost,1433;Initial Catalog=otus23;User Id=sa;Password=4c715ch5dtkqk23;MultipleActiveResultSets=True;");
        }
    }
}
