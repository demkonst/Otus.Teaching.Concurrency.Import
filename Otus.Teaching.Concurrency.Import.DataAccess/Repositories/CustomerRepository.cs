using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository: ICustomerRepository
    {
        private readonly AppDbContext _db = new();

        public void AddCustomer(Customer customer)
        {
            _db.Customers.Add(customer);
        }

        public void Reset()
        {
            _db.Customers.RemoveRange(_db.Customers);
            _db.SaveChanges();
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}
