using System.Collections.Generic;
using Bogus;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public static class RandomCustomerGenerator
    {
        private static readonly Faker<Customer> CustomersFaker = CreateFaker();

        public static List<Customer> Generate(int dataCount)
        {
            var customers = new List<Customer>();

            foreach (var customer in CustomersFaker.GenerateForever())
            {
                customers.Add(customer);

                if (dataCount == customer.Id)
                {
                    return customers;
                }
            }

            return customers;
        }

        private static Faker<Customer> CreateFaker()
        {
            var id = 1;
            var result = new Faker<Customer>()
                .CustomInstantiator(f => new Customer
                {
                    Id = id++
                })
                .RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return result;
        }
    }
}
